package com.renzomartinez.themoviedb;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.renzomartinez.themoviedb.presentation.base.BaseActivity;
import com.renzomartinez.themoviedb.presentation.movies.MoviesActivity;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(R.id.ivSplashLogo)
    ImageView ivSplashLogo;

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        Animation fadeinAnimation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.anim_fade_in);
        fadeinAnimation.setAnimationListener(animationListener);
        ivSplashLogo.startAnimation(fadeinAnimation);
    }

    Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            startActivity(new Intent(MainActivity.this, MoviesActivity.class));
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };
}
