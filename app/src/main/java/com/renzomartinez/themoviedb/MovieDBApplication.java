package com.renzomartinez.themoviedb;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.renzomartinez.themoviedb.domain.executor.MainThreadExecutor;
import com.renzomartinez.themoviedb.domain.executor.MainThreadExecutorImpl;
import com.renzomartinez.themoviedb.domain.executor.ThreadExecutor;

public class MovieDBApplication extends Application {

    public static MovieDBApplication instanceMovieDbApp;
    public static ThreadExecutor threadExecutor;
    public static MainThreadExecutor mainThreadExecutor;

    @Override
    public void onCreate() {
        super.onCreate();
        instanceMovieDbApp = this;
        initializeVariables();
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.getDefaultNightMode());
    }

    public static MovieDBApplication getInstance() {
        return instanceMovieDbApp;
    }

    private void initializeVariables() {
        threadExecutor = new ThreadExecutor();
        mainThreadExecutor = new MainThreadExecutorImpl();
    }
}
