package com.renzomartinez.themoviedb.utils;

public class ApiConstants {

    public static final String BASE_URL = "https://api.themoviedb.org";
    public static final String BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w500/";

    public static final String PATH_MOVIES = BASE_URL + "/4/list/1";
    public static final String PATH_MOVIE = BASE_URL + "/3/movie/{movie_id}";
    public static final String PATH_MOVIE_VIDEOS = BASE_URL + "/3/movie/{movie_id}/videos";
}
