package com.renzomartinez.themoviedb.domain.executor;

public interface MainThreadExecutor {
    void execute(Runnable runnable);
}
