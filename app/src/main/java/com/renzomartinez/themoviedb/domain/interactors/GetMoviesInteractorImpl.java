package com.renzomartinez.themoviedb.domain.interactors;

import com.renzomartinez.themoviedb.data.MoviesRepository;
import com.renzomartinez.themoviedb.data.datasource.exception.DataException;
import com.renzomartinez.themoviedb.data.repositories.MoviesRepositoryImpl;
import com.renzomartinez.themoviedb.domain.AbstractInteractor;
import com.renzomartinez.themoviedb.domain.GetMoviesInteractor;
import com.renzomartinez.themoviedb.domain.entities.MovieList;
import com.renzomartinez.themoviedb.domain.entities.mapper.MoviesMapper;

public class GetMoviesInteractorImpl extends AbstractInteractor implements GetMoviesInteractor {

    private Callback callback;
    private MoviesRepository moviesRepository;
    private MoviesMapper moviesMapper;

    private int page;

    public GetMoviesInteractorImpl() {
        moviesRepository = new MoviesRepositoryImpl();
        moviesMapper = new MoviesMapper();
    }

    @Override
    public void execute(int page, Callback callback) {
        this.page = page;
        this.callback = callback;
        getInteractorExecutor().run(GetMoviesInteractorImpl.this);
    }

    @Override
    public void run() {
        try{
            final MovieList movieList = moviesMapper.transformtMoviesList(moviesRepository.getMovies(page));
            getMainThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    callback.onGetMoviesSuccess(movieList);
                }
            });
        } catch (final DataException ex) {
            getMainThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    callback.onError(ex.getMessage());
                }
            });
        }
    }
}
