package com.renzomartinez.themoviedb.domain.entities.mapper;

import com.renzomartinez.themoviedb.data.entities.VideoData;
import com.renzomartinez.themoviedb.data.entities.VideoResponseData;
import com.renzomartinez.themoviedb.domain.entities.Video;

import java.util.ArrayList;

public class VideoMapper {

    public ArrayList<Video> transformVideoList(VideoResponseData data) {
        ArrayList<Video> videoList = new ArrayList<>();
        for(VideoData itemData : data.getResults()) {
            videoList.add(transformVideo(itemData));
        }
        return videoList;
    }

    private Video transformVideo(VideoData data) {

        Video video = new Video();
        video.setId(data.getId());
        video.setName(data.getName());
        video.setSite(data.getSite());
        video.setType(data.getType());
        video.setSize(data.getSize());
        video.setKey(data.getKey());

        return video;
    }
}
