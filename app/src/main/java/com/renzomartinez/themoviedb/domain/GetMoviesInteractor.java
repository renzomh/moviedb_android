package com.renzomartinez.themoviedb.domain;

import com.renzomartinez.themoviedb.domain.entities.MovieList;

public interface GetMoviesInteractor {

    void execute(int page, Callback callback);

    interface Callback {
        void onGetMoviesSuccess(MovieList movieList);
        void onError(String message);
    }
}
