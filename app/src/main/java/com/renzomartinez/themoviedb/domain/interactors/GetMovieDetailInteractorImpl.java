package com.renzomartinez.themoviedb.domain.interactors;

import com.renzomartinez.themoviedb.data.MoviesRepository;
import com.renzomartinez.themoviedb.data.datasource.exception.DataException;
import com.renzomartinez.themoviedb.data.repositories.MoviesRepositoryImpl;
import com.renzomartinez.themoviedb.domain.AbstractInteractor;
import com.renzomartinez.themoviedb.domain.GetMovieDetailInteractor;
import com.renzomartinez.themoviedb.domain.entities.MovieDetail;
import com.renzomartinez.themoviedb.domain.entities.mapper.MoviesMapper;

public class GetMovieDetailInteractorImpl extends AbstractInteractor implements GetMovieDetailInteractor {

    private Callback callback;
    private MoviesRepository moviesRepository;
    private MoviesMapper moviesMapper;

    private int movieId;

    public GetMovieDetailInteractorImpl() {
        moviesRepository = new MoviesRepositoryImpl();
        moviesMapper = new MoviesMapper();
    }

    @Override
    public void execute(int movieId, Callback callback) {
        this.movieId = movieId;
        this.callback = callback;
        getInteractorExecutor().run(GetMovieDetailInteractorImpl.this);
    }

    @Override
    public void run() {
        try{
            final MovieDetail movieDetail = moviesMapper.transformDetail(moviesRepository.getMovieDetail(movieId));
            getMainThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    callback.onGetMovieDetailSuccess(movieDetail);
                }
            });
        } catch (final DataException ex) {
            getMainThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    callback.onError(ex.getMessage());
                }
            });
        }
    }
}
