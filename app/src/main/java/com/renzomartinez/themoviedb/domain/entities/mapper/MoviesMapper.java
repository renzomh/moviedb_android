package com.renzomartinez.themoviedb.domain.entities.mapper;

import com.renzomartinez.themoviedb.data.entities.GenreData;
import com.renzomartinez.themoviedb.data.entities.MovieData;
import com.renzomartinez.themoviedb.data.entities.MovieDetailResponseData;
import com.renzomartinez.themoviedb.data.entities.MovieListResponseData;
import com.renzomartinez.themoviedb.data.entities.ProductionCompanyData;
import com.renzomartinez.themoviedb.data.entities.ProductionCountryData;
import com.renzomartinez.themoviedb.domain.entities.Movie;
import com.renzomartinez.themoviedb.domain.entities.MovieDetail;
import com.renzomartinez.themoviedb.domain.entities.MovieList;
import com.renzomartinez.themoviedb.utils.ApiConstants;

import java.util.ArrayList;

public class MoviesMapper {

    public MovieList transformtMoviesList(MovieListResponseData data) {

        MovieList movieList = new MovieList();
        movieList.setPage(data.getPage());
        movieList.setTotalPages(data.getTotalPages());
        movieList.setTotalResults(data.getTotalResults());
        movieList.setMovies(transformList(data.getResults()));

        return movieList;
    }

    private ArrayList<Movie> transformList(ArrayList<MovieData> data) {

        ArrayList<Movie> movieList = new ArrayList<>();
        if(data != null) {
            for(MovieData itemData : data) {
                Movie item = new Movie();
                item.setId(itemData.getId());
                item.setTitle(itemData.getTitle());
                item.setPosterPath(ApiConstants.BASE_IMAGE_URL + itemData.getPosterPath());
                movieList.add(item);
            }
        }
        return movieList;
    }

    public MovieDetail transformDetail(MovieDetailResponseData data) {

        MovieDetail movieDetail = new MovieDetail();

        movieDetail.setId(data.getId());
        movieDetail.setTitle(data.getTitle());
        movieDetail.setOriginalTitle(data.getOriginalTitle());
        movieDetail.setOverview(data.getOverview());
        movieDetail.setPosterPath(ApiConstants.BASE_IMAGE_URL + data.getPosterPath());
        movieDetail.setBackdropPath(ApiConstants.BASE_IMAGE_URL + data.getBackdropPath());
        movieDetail.setReleaseDate(data.getReleaseDate());

        ArrayList<String> genres = new ArrayList<>();
        for(GenreData itemData : data.getGenres()) {
            genres.add(itemData.getName());
        }
        movieDetail.setGenres(genres);

        ArrayList<String> productionCountries = new ArrayList<>();
        for(ProductionCountryData itemData : data.getProductionCountries()) {
            productionCountries.add(itemData.getName());
        }
        movieDetail.setProductionCountries(productionCountries);

        ArrayList<String> productionCompanies = new ArrayList<>();
        for(ProductionCompanyData itemData : data.getProductionCompanies()) {
            productionCompanies.add(itemData.getName());
        }
        movieDetail.setProductionCompanies(productionCompanies);

        return movieDetail;
    }
}
