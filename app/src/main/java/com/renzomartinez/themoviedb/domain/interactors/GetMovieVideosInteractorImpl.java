package com.renzomartinez.themoviedb.domain.interactors;

import com.renzomartinez.themoviedb.data.MoviesRepository;
import com.renzomartinez.themoviedb.data.datasource.exception.DataException;
import com.renzomartinez.themoviedb.data.repositories.MoviesRepositoryImpl;
import com.renzomartinez.themoviedb.domain.AbstractInteractor;
import com.renzomartinez.themoviedb.domain.GetMovieVideosInteractor;
import com.renzomartinez.themoviedb.domain.GetMoviesInteractor;
import com.renzomartinez.themoviedb.domain.entities.MovieList;
import com.renzomartinez.themoviedb.domain.entities.Video;
import com.renzomartinez.themoviedb.domain.entities.mapper.MoviesMapper;
import com.renzomartinez.themoviedb.domain.entities.mapper.VideoMapper;

import java.util.ArrayList;

public class GetMovieVideosInteractorImpl extends AbstractInteractor implements GetMovieVideosInteractor {

    private Callback callback;
    private MoviesRepository moviesRepository;
    private VideoMapper videoMapper;

    private int movieId;

    public GetMovieVideosInteractorImpl() {
        moviesRepository = new MoviesRepositoryImpl();
        videoMapper = new VideoMapper();
    }

    @Override
    public void execute(int movieId, Callback callback) {
        this.movieId = movieId;
        this.callback = callback;
        getInteractorExecutor().run(GetMovieVideosInteractorImpl.this);
    }

    @Override
    public void run() {
        try{
            final ArrayList<Video> videoList = videoMapper.transformVideoList(moviesRepository.getMovieVideos(movieId));
            getMainThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    callback.onGetMovieVideosSuccess(videoList);
                }
            });
        } catch (final DataException ex) {
            getMainThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    callback.onError(ex.getMessage());
                }
            });
        }
    }
}
