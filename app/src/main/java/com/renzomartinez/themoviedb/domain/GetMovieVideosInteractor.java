package com.renzomartinez.themoviedb.domain;

import com.renzomartinez.themoviedb.domain.entities.Video;

import java.util.ArrayList;

public interface GetMovieVideosInteractor {

    void execute(int movieId, Callback callback);

    interface Callback {
        void onGetMovieVideosSuccess(ArrayList<Video> videoList);
        void onError(String message);
    }
}
