package com.renzomartinez.themoviedb.domain;

import com.renzomartinez.themoviedb.domain.entities.MovieDetail;

public interface GetMovieDetailInteractor {

    void execute(int movieId, Callback callback);

    interface Callback {
        void onGetMovieDetailSuccess(MovieDetail movieDetail);
        void onError(String message);
    }
}
