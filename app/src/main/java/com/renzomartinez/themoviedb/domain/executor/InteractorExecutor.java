package com.renzomartinez.themoviedb.domain.executor;

public interface InteractorExecutor {
    void run(Interactor interactor);
}
