package com.renzomartinez.themoviedb.domain;

import com.renzomartinez.themoviedb.MovieDBApplication;
import com.renzomartinez.themoviedb.domain.executor.Interactor;
import com.renzomartinez.themoviedb.domain.executor.InteractorExecutor;
import com.renzomartinez.themoviedb.domain.executor.MainThreadExecutor;

public abstract class AbstractInteractor implements Interactor {

    private InteractorExecutor interactorExecutor;
    private MainThreadExecutor mainThreadExecutor;

    public AbstractInteractor() {
        this.interactorExecutor = MovieDBApplication.threadExecutor;
        this.mainThreadExecutor = MovieDBApplication.mainThreadExecutor;
    }

    public InteractorExecutor getInteractorExecutor() {
        return interactorExecutor;
    }

    public MainThreadExecutor getMainThreadExecutor() {
        return mainThreadExecutor;
    }
}
