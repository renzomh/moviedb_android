package com.renzomartinez.themoviedb.presentation.settings;

import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.renzomartinez.themoviedb.R;
import com.renzomartinez.themoviedb.presentation.base.BaseActivity;
import com.renzomartinez.themoviedb.utils.Utils;

import butterknife.BindView;

public class SettingsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.themeGroup)
    RadioGroup themeGroup;
    @BindView(R.id.themeDark)
    RadioButton themeDark;
    @BindView(R.id.themeLight)
    RadioButton themeLight;
    @BindView(R.id.themeBattery)
    RadioButton themeBattery;
    @BindView(R.id.themeSystem)
    RadioButton themeSystem;

    @Override
    protected int getLayout() {
        return R.layout.activity_settings;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        setupToolbar(toolbar, getString(R.string.menu_settings_title), true);
        initThemeListener();
        initTheme();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
                default:
                    return super.onOptionsItemSelected(item);
        }
    }

    private void initTheme() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P){
            themeSystem.setVisibility(View.VISIBLE);
        } else {
            themeSystem.setVisibility(View.GONE);
        }

        switch (Utils.getSavedTheme(getApplicationContext())) {

            case Utils.THEME_LIGHT:
                themeLight.setChecked(true);
                break;
            case Utils.THEME_DARK:
                themeDark.setChecked(true);
                break;
            case Utils.THEME_BATTERY:
                themeBattery.setChecked(true);
                break;
            case Utils.THEME_UNDEFINED:

                int currentNightMode = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;

                switch (currentNightMode) {
                    case Configuration.UI_MODE_NIGHT_NO:
                    case Configuration.UI_MODE_NIGHT_UNDEFINED:
                        themeLight.setChecked(true);
                        break;
                    case Configuration.UI_MODE_NIGHT_YES:
                        themeDark.setChecked(true);
                        break;
                }

                break;
        }
    }

    private void initThemeListener() {
        themeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.themeLight:
                        setTheme(AppCompatDelegate.MODE_NIGHT_NO, Utils.THEME_LIGHT);
                        break;
                    case R.id.themeDark:
                        setTheme(AppCompatDelegate.MODE_NIGHT_YES, Utils.THEME_DARK);
                        break;
                    case R.id.themeBattery:
                        setTheme(AppCompatDelegate.MODE_NIGHT_AUTO, Utils.THEME_BATTERY);
                        break;
                    case R.id.themeSystem:
                        setTheme(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM, Utils.THEME_SYSTEM);
                        break;
                }
            }
        });
    }

    private void setTheme(int themeMode, int prefMode) {
        AppCompatDelegate.setDefaultNightMode(themeMode);
        getDelegate().applyDayNight();
        Utils.saveTheme(getApplicationContext(), prefMode);
    }
}
