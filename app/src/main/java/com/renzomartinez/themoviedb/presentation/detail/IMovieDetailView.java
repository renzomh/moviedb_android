package com.renzomartinez.themoviedb.presentation.detail;

import com.renzomartinez.themoviedb.domain.entities.MovieDetail;
import com.renzomartinez.themoviedb.domain.entities.MovieList;
import com.renzomartinez.themoviedb.domain.entities.Video;
import com.renzomartinez.themoviedb.presentation.base.BaseView;

import java.util.ArrayList;

public interface IMovieDetailView extends BaseView {
    void onGetMoviesSuccess(MovieDetail movieDetail);
    void onGetVideosSuccess(ArrayList<Video> videoList);
}
