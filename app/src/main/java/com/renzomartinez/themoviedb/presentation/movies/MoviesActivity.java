package com.renzomartinez.themoviedb.presentation.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.renzomartinez.themoviedb.R;
import com.renzomartinez.themoviedb.domain.entities.Movie;
import com.renzomartinez.themoviedb.domain.entities.MovieList;
import com.renzomartinez.themoviedb.presentation.base.BaseActivity;
import com.renzomartinez.themoviedb.presentation.base.BaseLoadMoreListener;
import com.renzomartinez.themoviedb.presentation.detail.MovieDetailActivity;
import com.renzomartinez.themoviedb.presentation.settings.SettingsActivity;
import com.renzomartinez.themoviedb.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;

public class MoviesActivity extends BaseActivity implements IMoviesView, MovieItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvMovies)
    RecyclerView rvMovies;

    private IMovieListPresenter presenter;
    private MoviesListAdapter listAdapter;

    private int page;
    private ArrayList<Movie> listMovies;

    @Override
    protected int getLayout() {
        return R.layout.activity_movies;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        Utils.initTheme(getApplicationContext());
        setupToolbar(toolbar, getString(R.string.movies_title), false);
        setupLoader();
        setupRecyclerView();
        presenter = new MovieListPresenter();
        presenter.onAttach(this);
        presenter.getMovies(page);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        recreate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent intentSettings = new Intent(this, SettingsActivity.class);
                startActivity(intentSettings);
                return true;
                default:
                    return super.onOptionsItemSelected(item);
        }

    }

    private void setupRecyclerView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvMovies.setLayoutManager(linearLayoutManager);

        listAdapter = new MoviesListAdapter(getApplicationContext());
        listAdapter.setItemClickListener(this);
        listAdapter.setLoadMoreListener(new BaseLoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadMoreData();
            }
        });

        page = 1;
        listMovies = new ArrayList<>();
        listAdapter.setProductList(listMovies);
        rvMovies.setAdapter(listAdapter);

    }

    @Override
    public void onGetMoviesSuccess(MovieList movieList) {
        listAdapter.setMoreDataAvailable(movieList.getPage() < movieList.getTotalPages());
        if (movieList.getMovies() != null && movieList.getMovies().size() > 0) {
            listMovies.addAll(movieList.getMovies());
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onMovieClickListener(int movieId) {
        Intent intentDetail = new Intent(MoviesActivity.this, MovieDetailActivity.class);
        intentDetail.putExtra(MovieDetailActivity.ARG_MOVIE_ID, movieId);
        startActivity(intentDetail);
    }

    private void loadMoreData() {
        page++;
        presenter.getMovies(page);
    }
}
