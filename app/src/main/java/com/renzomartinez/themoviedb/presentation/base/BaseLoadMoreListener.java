package com.renzomartinez.themoviedb.presentation.base;

public interface BaseLoadMoreListener {
    void onLoadMore();
}
