package com.renzomartinez.themoviedb.presentation.base;

public interface BaseView {
    void startLoading();
    void stopLoading();
    void showErrorDialog(String message);
}
