package com.renzomartinez.themoviedb.presentation.movies;

import com.renzomartinez.themoviedb.domain.GetMoviesInteractor;
import com.renzomartinez.themoviedb.domain.entities.MovieList;
import com.renzomartinez.themoviedb.domain.interactors.GetMoviesInteractorImpl;

public class MovieListPresenter implements IMovieListPresenter {

    private IMoviesView view;
    private GetMoviesInteractor moviesInteractor;

    public MovieListPresenter() {
        moviesInteractor = new GetMoviesInteractorImpl();
    }

    @Override
    public void getMovies(int page) {
        if(view == null) return;

        view.startLoading();
        moviesInteractor.execute(page, new GetMoviesInteractor.Callback() {
            @Override
            public void onGetMoviesSuccess(MovieList movieList) {
                if(view != null) {
                    view.stopLoading();
                    view.onGetMoviesSuccess(movieList);
                }
            }

            @Override
            public void onError(String message) {
                if(view != null) {
                    view.stopLoading();
                    view.showErrorDialog(message);
                }
            }
        });
    }

    @Override
    public void onAttach(IMoviesView view) {
        this.view = view;
    }

    @Override
    public void onDetach() {
        this.view = null;
    }
}
