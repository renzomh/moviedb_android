package com.renzomartinez.themoviedb.presentation.movies;

public interface MovieItemClickListener {
    void onMovieClickListener(int movieId);
}
