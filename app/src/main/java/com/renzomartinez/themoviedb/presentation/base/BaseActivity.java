package com.renzomartinez.themoviedb.presentation.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.renzomartinez.themoviedb.R;
import com.renzomartinez.themoviedb.presentation.view.LoaderDialog;
import com.renzomartinez.themoviedb.presentation.view.MessageDialog;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    protected LoaderDialog mProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        injectViews();
        setupView(savedInstanceState);
    }

    protected void setupToolbar(Toolbar toolBar, String title, boolean showHome) {
        setSupportActionBar(toolBar);
        TextView tvTitle = toolBar.findViewById(R.id.tvToolbarTitle);
        tvTitle.setText(title);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(showHome);
        getSupportActionBar().setDisplayShowHomeEnabled(showHome);
    }

    private void injectViews() {
        ButterKnife.bind(this);
    }

    public void setupLoader() {
        mProgress = new LoaderDialog(this);
    }

    @Override
    public void startLoading() {
        if (mProgress != null) {
            mProgress.show();
        }
    }

    @Override
    public void stopLoading() {
        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    @Override
    public void showErrorDialog(String message) {
        MessageDialog.showMessageDialog(this, "¡Vaya!", message);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mProgress != null) {
            mProgress.dismiss();
            mProgress = null;
        }
    }

    protected abstract int getLayout();

    protected abstract void setupView(Bundle savedInstanceState);
}
