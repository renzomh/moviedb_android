package com.renzomartinez.themoviedb.presentation.detail;

import com.renzomartinez.themoviedb.presentation.base.BasePresenter;
import com.renzomartinez.themoviedb.presentation.movies.IMoviesView;

public interface IMovieDetailPresenter extends BasePresenter<IMovieDetailView> {
    void getMovieDetail(int movieId);
    void getMovieVideos(int movieId);
}
