package com.renzomartinez.themoviedb.presentation.detail;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.renzomartinez.themoviedb.R;
import com.renzomartinez.themoviedb.domain.entities.MovieDetail;
import com.renzomartinez.themoviedb.domain.entities.Video;
import com.renzomartinez.themoviedb.presentation.base.BaseActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class MovieDetailActivity extends BaseActivity implements IMovieDetailView {

    public static final String ARG_MOVIE_ID = "arg_movie_id";

    @BindView(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsing_toolbar_layout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ivDetailBackdrop)
    ImageView ivDetailBackdrop;
    @BindView(R.id.ivDetailPoster)
    ImageView ivDetailPoster;
    @BindView(R.id.tvDetailName)
    TextView tvDetailName;
    @BindView(R.id.tvDetailRelease)
    TextView tvDetailRelease;
    @BindView(R.id.tvDetailOverview)
    TextView tvDetailOverview;
    @BindView(R.id.tvDetailGenres)
    TextView tvDetailGenres;
    @BindView(R.id.tvDetailCountries)
    TextView tvDetailCountries;
    @BindView(R.id.tvDetailCompanies)
    TextView tvDetailCompanies;

    private IMovieDetailPresenter presenter;
    private ArrayList<Video> videoList;

    @Override
    protected int getLayout() {
        return R.layout.activity_movie_detail;
    }

    @Override
    protected void setupView(Bundle savedInstanceState) {
        setupLoader();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        presenter = new MovieDetailPresenter();
        presenter.onAttach(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            int movieId = extras.getInt(ARG_MOVIE_ID, 0);
            presenter.getMovieDetail(movieId);
            presenter.getMovieVideos(movieId);
        }
    }

    @Override
    public void onGetMoviesSuccess(MovieDetail movieDetail) {
        if(movieDetail != null) {
            collapsing_toolbar_layout.setTitle(movieDetail.getTitle());
            //Glide.with(getApplicationContext()).load(movieDetail.getBackdropPath()).placeholder(R.drawable.ic_placeholder_horizontal).fit().centerCrop().into(ivDetailBackdrop);
            //Glide.with(getApplicationContext()).load(movieDetail.getPosterPath()).placeholder(R.drawable.ic_placeholder_vertical).fit().centerCrop().into(ivDetailPoster);
            tvDetailName.setText(movieDetail.getOriginalTitle());
            tvDetailRelease.setText(movieDetail.getReleaseDate());
            tvDetailOverview.setText(movieDetail.getOverview());
            tvDetailGenres.setText(getArrayToString(movieDetail.getGenres()));
            tvDetailCompanies.setText(getArrayToString(movieDetail.getProductionCompanies()));
            tvDetailCountries.setText(getArrayToString(movieDetail.getProductionCountries()));

            Glide.with(this).
                    load(movieDetail.getBackdropPath()).
                    apply(RequestOptions.placeholderOf(ContextCompat.getDrawable(this, R.drawable.ic_placeholder_horizontal)).centerCrop()).
                    into(ivDetailBackdrop);

            Glide.with(this).
                    load(movieDetail.getPosterPath()).
                    apply(RequestOptions.placeholderOf(ContextCompat.getDrawable(this, R.drawable.ic_placeholder_vertical)).centerCrop()).
                    into(ivDetailPoster);
        }
    }

    @Override
    public void onGetVideosSuccess(ArrayList<Video> videoList) {
        this.videoList = videoList;
    }

    @OnClick(R.id.btnDetailTrailer)
    public void onClickTrailer() {
        if(videoList != null && videoList.size() > 0) {
            if(videoList.size() == 1) {
                navigateToVideo(videoList.get(0));
            } else {
                showVideosDialog();
            }
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.detail_no_videos), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }

    private void navigateToVideo(Video video) {
        Intent intentVideo = new Intent(this, MovieVideoActivity.class);
        intentVideo.putExtra(MovieVideoActivity.ARG_LINK, video.getKey());
        startActivity(intentVideo);
    }

    private void showVideosDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MovieDetailActivity.this);
        builder.setTitle(getString(R.string.detail_choose_video));

        builder.setItems(getArrayListToArray(videoList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                navigateToVideo(videoList.get(which));
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private String[] getArrayListToArray(ArrayList<Video> list) {
        String[] videos = new String[list.size()];
        for(int i=0; i<list.size(); i++) {
            videos[i] = list.get(i).getName();
        }
        return videos;
    }

    private String getArrayToString(ArrayList<String> list) {
        String message = "";
        for(int i = 0; i<list.size(); i++) {
            message = message + list.get(i);
            if(i != list.size() - 1) {
                message = message + ", ";
            }
        }
        return message;
    }
}
