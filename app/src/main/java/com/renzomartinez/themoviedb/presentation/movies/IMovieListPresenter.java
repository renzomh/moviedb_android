package com.renzomartinez.themoviedb.presentation.movies;

import com.renzomartinez.themoviedb.presentation.base.BasePresenter;

public interface IMovieListPresenter extends BasePresenter<IMoviesView> {
    void getMovies(int page);
}
