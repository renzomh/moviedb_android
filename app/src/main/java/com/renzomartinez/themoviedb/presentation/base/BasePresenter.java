package com.renzomartinez.themoviedb.presentation.base;

public interface BasePresenter<T> {
    void onAttach(T view);
    void onDetach();
}
