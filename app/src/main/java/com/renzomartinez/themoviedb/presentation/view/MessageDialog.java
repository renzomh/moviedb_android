package com.renzomartinez.themoviedb.presentation.view;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;

import com.renzomartinez.themoviedb.R;

public class MessageDialog {

    public static void showMessageDialog(Context context, String message) {
        AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(
                context, R.style.AppCompatAlertDialogStyle)).setTitle(context.getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.dialog_message_positive), null).create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showMessageDialog(Context context, String title, String message) {
        AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(
                context, R.style.AppCompatAlertDialogStyle)).setTitle(title)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.dialog_message_positive), null).create();
        dialog.setCancelable(false);
        dialog.show();
    }
}