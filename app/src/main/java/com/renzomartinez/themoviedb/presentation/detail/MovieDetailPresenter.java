package com.renzomartinez.themoviedb.presentation.detail;

import com.renzomartinez.themoviedb.domain.GetMovieDetailInteractor;
import com.renzomartinez.themoviedb.domain.GetMovieVideosInteractor;
import com.renzomartinez.themoviedb.domain.GetMoviesInteractor;
import com.renzomartinez.themoviedb.domain.entities.MovieDetail;
import com.renzomartinez.themoviedb.domain.entities.MovieList;
import com.renzomartinez.themoviedb.domain.entities.Video;
import com.renzomartinez.themoviedb.domain.interactors.GetMovieDetailInteractorImpl;
import com.renzomartinez.themoviedb.domain.interactors.GetMovieVideosInteractorImpl;
import com.renzomartinez.themoviedb.domain.interactors.GetMoviesInteractorImpl;
import com.renzomartinez.themoviedb.presentation.movies.IMovieListPresenter;
import com.renzomartinez.themoviedb.presentation.movies.IMoviesView;

import java.util.ArrayList;

public class MovieDetailPresenter implements IMovieDetailPresenter {

    private IMovieDetailView view;
    private GetMovieDetailInteractor detailInteractor;
    private GetMovieVideosInteractor videosInteractor;

    public MovieDetailPresenter() {
        detailInteractor = new GetMovieDetailInteractorImpl();
        videosInteractor = new GetMovieVideosInteractorImpl();
    }

    @Override
    public void getMovieDetail(int movieId) {
        if(view == null) return;

        view.startLoading();
        detailInteractor.execute(movieId, new GetMovieDetailInteractor.Callback() {
            @Override
            public void onGetMovieDetailSuccess(MovieDetail movieDetail) {
                if(view != null) {
                    view.stopLoading();
                    view.onGetMoviesSuccess(movieDetail);
                }
            }

            @Override
            public void onError(String message) {
                if(view != null) {
                    view.stopLoading();
                    view.showErrorDialog(message);
                }
            }
        });
    }

    @Override
    public void getMovieVideos(int movieId) {
        if(view == null) return;

        videosInteractor.execute(movieId, new GetMovieVideosInteractor.Callback() {

            @Override
            public void onGetMovieVideosSuccess(ArrayList<Video> videoList) {
                if(view != null) {
                    view.stopLoading();
                    view.onGetVideosSuccess(videoList);
                }
            }

            @Override
            public void onError(String message) {
                if(view != null) {
                    view.stopLoading();
                    view.showErrorDialog(message);
                }
            }
        });
    }

    @Override
    public void onAttach(IMovieDetailView view) {
        this.view = view;
    }

    @Override
    public void onDetach() {
        this.view = null;
    }
}
