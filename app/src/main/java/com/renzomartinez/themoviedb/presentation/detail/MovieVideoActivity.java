package com.renzomartinez.themoviedb.presentation.detail;

import android.os.Bundle;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.renzomartinez.themoviedb.BuildConfig;
import com.renzomartinez.themoviedb.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieVideoActivity extends YouTubeBaseActivity {

    public static final String ARG_LINK = "arg_link";

    @BindView(R.id.player)
    YouTubePlayerView playerVideo;

    private String url;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_movie_video);

        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            url = extras.getString(ARG_LINK, "");

            playerVideo.initialize(BuildConfig.GOOGLE_API_KEY, new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                    youTubePlayer.loadVideo(url);
                }
                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                    YouTubeInitializationResult youTubeInitializationResult) {

                }
            });
        }
    }
}
