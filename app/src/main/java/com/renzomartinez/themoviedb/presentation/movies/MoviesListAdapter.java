package com.renzomartinez.themoviedb.presentation.movies;

import android.content.Context;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.renzomartinez.themoviedb.R;
import com.renzomartinez.themoviedb.domain.entities.Movie;
import com.renzomartinez.themoviedb.presentation.base.BaseLoadHolder;
import com.renzomartinez.themoviedb.presentation.base.BaseLoadMoreListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_ITEM = 0;
    public static final int TYPE_LOAD = 1;

    private ArrayList<Movie> movieList;
    private Context mContext;
    private MovieItemClickListener itemClickListener;
    private BaseLoadMoreListener loadMoreListener;
    private boolean isMoreDataAvailable = true;

    public MoviesListAdapter(Context mContext) {
        this.mContext = mContext;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_ITEM) {
            return new MovieViewHolder(inflater.inflate(R.layout.item_movie_list, parent, false));
        } else {
            return new BaseLoadHolder(inflater.inflate(R.layout.item_load, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position >= getItemCount() - 1 && isMoreDataAvailable && loadMoreListener != null) {
            loadMoreListener.onLoadMore();
        }
        if (getItemViewType(position) == TYPE_ITEM) {
            Movie movie = movieList.get(position);
            MovieViewHolder movieHolder = (MovieViewHolder) holder;
            movieHolder.tvItemMovieListName.setText(movie.getTitle());
            //Picasso.with(mContext).load(movie.getPosterPath()).fit().centerCrop().into(movieHolder.ivItemMovieList);
            Glide.with(mContext).
                    load(movie.getPosterPath()).
                    apply(RequestOptions.placeholderOf(ContextCompat.getDrawable(mContext, R.drawable.ic_placeholder_vertical)).centerCrop()).
                    into(movieHolder.ivItemMovieList);

            //ContextCompat.getDrawable(context, R.drawable.some_drawble)
        }
    }

    class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.cardItemMovieList)
        CardView cardItemMovieList;
        @BindView(R.id.ivItemMovieList)
        ImageView ivItemMovieList;
        @BindView(R.id.tvItemMovieListName)
        TextView tvItemMovieListName;

        public MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cardItemMovieList.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                if (v.getId() == R.id.cardItemMovieList) {
                    itemClickListener.onMovieClickListener(movieList.get(getAdapterPosition()).getId());
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return movieList != null ? movieList.size() : 0;
    }

    public void setProductList(ArrayList<Movie> movieList) {
        this.movieList = movieList;
    }

    public void setItemClickListener(MovieItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setLoadMoreListener(BaseLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }
}
