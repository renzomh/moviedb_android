package com.renzomartinez.themoviedb.presentation.movies;

import com.renzomartinez.themoviedb.domain.entities.MovieList;
import com.renzomartinez.themoviedb.presentation.base.BaseView;

public interface IMoviesView extends BaseView {
    void onGetMoviesSuccess(MovieList movieList);
}
