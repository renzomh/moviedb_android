package com.renzomartinez.themoviedb.data.entities;

import com.google.gson.annotations.SerializedName;

public class ErrorResponseData {

    public static final int ERROR_500 = 500;
    public static final int ERROR_404 = 404;
    public static final int ERROR_VALUE = -100;
    public static final int ERROR_PARSE = -101;
    public static final int ERROR_TIMEOUT = -102;
    public static final int ERROR_CONNECTION = -103;
    public static final int ERROR_GENERAL = -104;

    @SerializedName("status_code")
    private int code;
    @SerializedName("status_message")
    private String message;
    @SerializedName("success")
    private boolean success;

    public ErrorResponseData() {
    }

    public ErrorResponseData(int code, String message) {
        this.message = message;
        this.code = code;
        this.success = false;
    }

    public ErrorResponseData(int codError) {

        this.code = codError;
        this.success = false;

        switch (codError){

            case ERROR_404:
                this.message = "Mensaje 404";
                break;

            case ERROR_500:
                message = "Mensaje 500";
                break;

            case ERROR_VALUE:
                message = "Error al obtener los datos del servidor";
                break;

            case ERROR_PARSE:
                message = "Error al parsear los datos.";
                break;

            case ERROR_TIMEOUT:
                message = "Error de timeout";
                break;

            case ERROR_CONNECTION:
                message = "Error de conexión";
                break;

            case ERROR_GENERAL:
                message = "Ocurrió un error";
                break;

            default:
                message = "Mensaje error desconocido";
                break;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
