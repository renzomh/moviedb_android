package com.renzomartinez.themoviedb.data.repositories;

import com.renzomartinez.themoviedb.data.MoviesRepository;
import com.renzomartinez.themoviedb.data.datasource.DataStoreEnum;
import com.renzomartinez.themoviedb.data.datasource.datastore.MoviesDataStore;
import com.renzomartinez.themoviedb.data.datasource.exception.DataException;
import com.renzomartinez.themoviedb.data.datasource.factory.MoviesDataStoreFactory;
import com.renzomartinez.themoviedb.data.entities.MovieDetailResponseData;
import com.renzomartinez.themoviedb.data.entities.MovieListResponseData;
import com.renzomartinez.themoviedb.data.entities.VideoResponseData;

public class MoviesRepositoryImpl implements MoviesRepository {

    private final MoviesDataStoreFactory moviesDataStoreFactory;

    public MoviesRepositoryImpl() {
        this.moviesDataStoreFactory = new MoviesDataStoreFactory();
    }

    @Override
    public MovieListResponseData getMovies(int page) throws DataException {
        MoviesDataStore moviesDataStore = moviesDataStoreFactory.create(DataStoreEnum.REST);
        return moviesDataStore.getMovies(page);
    }

    @Override
    public MovieDetailResponseData getMovieDetail(int movieId) throws DataException {
        MoviesDataStore moviesDataStore = moviesDataStoreFactory.create(DataStoreEnum.REST);
        return moviesDataStore.getMovieDetail(movieId);
    }

    @Override
    public VideoResponseData getMovieVideos(int movieId) throws DataException {
        MoviesDataStore moviesDataStore = moviesDataStoreFactory.create(DataStoreEnum.REST);
        return moviesDataStore.getMovieVideos(movieId);
    }
}
