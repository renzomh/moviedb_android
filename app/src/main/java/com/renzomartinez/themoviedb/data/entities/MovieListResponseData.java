package com.renzomartinez.themoviedb.data.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MovieListResponseData {

    @SerializedName("page")
    private int page;
    @SerializedName("total_pages")
    private int totalPages;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("results")
    private ArrayList<MovieData> results;

    public int getPage() {
        return page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public ArrayList<MovieData> getResults() {
        return results;
    }
}
