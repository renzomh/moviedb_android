package com.renzomartinez.themoviedb.data.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class VideoResponseData {

    @SerializedName("id")
    private int id;
    @SerializedName("results")
    ArrayList<VideoData> results;

    public int getId() {
        return id;
    }

    public ArrayList<VideoData> getResults() {
        return results;
    }
}
