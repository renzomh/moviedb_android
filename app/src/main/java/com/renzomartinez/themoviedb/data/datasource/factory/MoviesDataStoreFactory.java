package com.renzomartinez.themoviedb.data.datasource.factory;

import com.renzomartinez.themoviedb.data.datasource.DataStoreEnum;
import com.renzomartinez.themoviedb.data.datasource.datastore.MoviesDataStore;
import com.renzomartinez.themoviedb.data.datasource.rest.MoviesRestDataStore;

public class MoviesDataStoreFactory {
    public MoviesDataStore create(DataStoreEnum dataStoreEnum) {
        MoviesDataStore moviesDataStore = null;
        switch (dataStoreEnum) {
            case REST:
                moviesDataStore = new MoviesRestDataStore();
                break;
            case DB:
                //TODO When we have get movie from any database
                break;
        }
        return moviesDataStore;
    }
}
