package com.renzomartinez.themoviedb.data.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MovieDetailData extends MovieBaseData {

    @SerializedName("backdrop_path")
    private String backdropPath;
    @SerializedName("genres")
    private ArrayList<GenreData> genres;
    @SerializedName("production_companies")
    private ArrayList<ProductionCompanyData> productionCompanies;
    @SerializedName("production_countries")
    private ArrayList<ProductionCountryData> productionCountries;

    public String getBackdropPath() {
        return backdropPath;
    }

    public ArrayList<GenreData> getGenres() {
        return genres;
    }

    public ArrayList<ProductionCompanyData> getProductionCompanies() {
        return productionCompanies;
    }

    public ArrayList<ProductionCountryData> getProductionCountries() {
        return productionCountries;
    }
}
