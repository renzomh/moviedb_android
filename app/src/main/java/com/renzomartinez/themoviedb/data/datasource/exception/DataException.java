package com.renzomartinez.themoviedb.data.datasource.exception;

public class DataException extends Exception {
    public DataException(String message) {
        super(message);
    }
}
