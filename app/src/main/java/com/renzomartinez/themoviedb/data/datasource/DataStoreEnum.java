package com.renzomartinez.themoviedb.data.datasource;

public enum DataStoreEnum {
    REST, DB
}
