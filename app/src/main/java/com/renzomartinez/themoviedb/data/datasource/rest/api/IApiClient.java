package com.renzomartinez.themoviedb.data.datasource.rest.api;

import com.renzomartinez.themoviedb.data.entities.MovieDetailResponseData;
import com.renzomartinez.themoviedb.data.entities.MovieListResponseData;
import com.renzomartinez.themoviedb.data.entities.VideoResponseData;
import com.renzomartinez.themoviedb.utils.ApiConstants;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface IApiClient {

    @GET(ApiConstants.PATH_MOVIES)
    Call<MovieListResponseData> getMovies(@QueryMap Map<String, Object> options);

    @GET(ApiConstants.PATH_MOVIE)
    Call<MovieDetailResponseData> getMovieDetail(@Path("movie_id") int movieId, @QueryMap Map<String, String> options);

    @GET(ApiConstants.PATH_MOVIE_VIDEOS)
    Call<VideoResponseData> getVideosByMovie(@Path("movie_id") int movieId, @QueryMap Map<String, String> options);
}
