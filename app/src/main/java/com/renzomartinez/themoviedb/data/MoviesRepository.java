package com.renzomartinez.themoviedb.data;

import com.renzomartinez.themoviedb.data.datasource.exception.DataException;
import com.renzomartinez.themoviedb.data.entities.MovieDetailResponseData;
import com.renzomartinez.themoviedb.data.entities.MovieListResponseData;
import com.renzomartinez.themoviedb.data.entities.VideoResponseData;

public interface MoviesRepository {
    MovieListResponseData getMovies(int page) throws DataException;
    MovieDetailResponseData getMovieDetail(int movieId) throws DataException;
    VideoResponseData getMovieVideos(int movieId) throws DataException;
}
