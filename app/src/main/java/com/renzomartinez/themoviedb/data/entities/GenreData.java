package com.renzomartinez.themoviedb.data.entities;

import com.google.gson.annotations.SerializedName;

public class GenreData {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
