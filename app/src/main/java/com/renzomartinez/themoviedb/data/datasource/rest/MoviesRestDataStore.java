package com.renzomartinez.themoviedb.data.datasource.rest;

import com.renzomartinez.themoviedb.BuildConfig;
import com.renzomartinez.themoviedb.data.datasource.datastore.MoviesDataStore;
import com.renzomartinez.themoviedb.data.datasource.exception.DataException;
import com.renzomartinez.themoviedb.data.datasource.rest.api.ApiClient;
import com.renzomartinez.themoviedb.data.entities.MovieDetailResponseData;
import com.renzomartinez.themoviedb.data.entities.MovieListResponseData;
import com.renzomartinez.themoviedb.data.entities.VideoResponseData;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Response;

public class MoviesRestDataStore implements MoviesDataStore {

    @Override
    public MovieListResponseData getMovies(int page) throws DataException {

        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put("page", page);
            map.put("api_key", BuildConfig.MOVIE_API_KEY);

            Response<MovieListResponseData> response = ApiClient.getApiClient().getMovies(map).execute();
            if(response.isSuccessful()){
                return response.body();
            } else {
                throw new DataException("Ocurrió un error. Intentelo nuevamente");
            }
        } catch (IOException e) {
            throw new DataException("Ocurrió un error. Intentelo nuevamente");
        }
    }

    @Override
    public MovieDetailResponseData getMovieDetail(int movieId) throws DataException {
        try {
            HashMap<String, String> map = new HashMap<>();
            map.put("api_key", BuildConfig.MOVIE_API_KEY);

            Response<MovieDetailResponseData> response = ApiClient.getApiClient().getMovieDetail(movieId, map).execute();
            if(response.isSuccessful()){
                return response.body();
            } else {
                throw new DataException("Ocurrió un error. Intentelo nuevamente");
            }
        } catch (IOException e) {
            throw new DataException("Ocurrió un error. Intentelo nuevamente");
        }
    }

    @Override
    public VideoResponseData getMovieVideos(int movieId) throws DataException {
        try {
            HashMap<String, String> map = new HashMap<>();
            map.put("api_key", BuildConfig.MOVIE_API_KEY);

            Response<VideoResponseData> response = ApiClient.getApiClient().getVideosByMovie(movieId, map).execute();
            if(response.isSuccessful()){
                return response.body();
            } else {
                throw new DataException("Ocurrió un error. Intentelo nuevamente");
            }
        } catch (IOException e) {
            throw new DataException("Ocurrió un error. Intentelo nuevamente");
        }
    }
}
