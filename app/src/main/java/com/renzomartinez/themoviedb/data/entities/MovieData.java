package com.renzomartinez.themoviedb.data.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MovieData extends MovieBaseData {

    @SerializedName("media_type")
    private String mediaType;
    @SerializedName("genre_ids")
    private ArrayList<Integer> genreIds;

    public String getMediaType() {
        return mediaType;
    }

    public ArrayList<Integer> getGenreIds() {
        return genreIds;
    }
}
